package controllers

import _root_.models.Task
import play.api._
import play.api.mvc._
import models._
import se.radley.plugin.salat._
import com.mongodb.casbah.Imports._
import com.novus.salat._
import play.api.data._
import play.api.data.Forms._

object Application extends Controller {
  val taskForm = Form(
    "label" -> nonEmptyText
  )

  def index = Action {
    Redirect(routes.Application.tasks)
  }

  def tasks() = Action {
    val tasks = Task.findAll
    Ok(views.html.list(tasks.toList,taskForm))
  }

  def newTask = Action { implicit request =>
    taskForm.bindFromRequest.fold(
      errors => BadRequest(views.html.list(Task.findAll.toList,errors)),
      label => {
        val task = Task(label=label)
        Task.save(task)
        Redirect(routes.Application.tasks)
      }
    )
  }

  def deleteTask(id: ObjectId) = Action {
    Task.removeById(id)
    Redirect(routes.Application.tasks)
  }

  def view(id: ObjectId) = Action {
    Task.findOneById(id).map( task =>
      Ok(views.html.task(task))
    ).getOrElse(NotFound)
  }
}
