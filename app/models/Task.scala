package models

import play.api.Play.current
import java.util.Date
import com.novus.salat._
import com.novus.salat.annotations._
import com.novus.salat.dao._
import com.mongodb.casbah.Imports._
import se.radley.plugin.salat._
import mongoContext._

case class Task(
  id: ObjectId = new ObjectId,
  label: String,
  added: Date = new Date(),
  updated: Option[Date] = None,
  deleted: Option[Date] = None
)

object Task extends ModelCompanion[Task, ObjectId] {
  val dao = new SalatDAO[Task, ObjectId](collection = mongoCollection("tasks")) {}
}
